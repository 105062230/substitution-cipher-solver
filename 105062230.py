from pycipher import SimpleSubstitution as SimpleSub  
import random
import re
from math import log10

class dictionary_score(object):
    def __init__(self,ngramfile):
        f = open(ngramfile,'r')
        self.dict = self.make_dict(f)
        self.L = 4
        self.sum = sum(self.dict.values())
        self.probabilities()
        self.floor = log10(0.01/self.sum)

    
    def make_dict(self,file):
        dic = {}
        for line in file.readlines():
            key,count = line.split(' ')
            dic[key] = int(count)
        return dic
    

    def probabilities(self):
        for key in self.dict.keys():
            f = (float(self.dict[key])/self.sum)
            self.dict[key] = log10(f)


def n_iter(x):return iter(range(x))

def cal_score(text, n):
    score = 0
    local_n = n.dict
    for i in n_iter(len(text)-n.L+1):
        s = text[i:i+n.L]
        if s in n.dict: score += local_n[s]
        else: score += n.floor
    return score


def random_swap(key):
    a = random.randint(0,25)
    b = random.randint(0,25)
    key_swap = key[:]
    key_swap[a],key_swap[b] = key_swap[b],key_swap[a]
    return key_swap


def get_key_score(key,ctext,n):
    deciphered = SimpleSub(key).decipher(ctext)
    return cal_score(deciphered,n)


def read_ciphertext(text_location):
    f = open(text_location,'r')
    inp = f.read()
    p1 = re.compile(r'[{](.*?)[}]', re.S)
    cflag = re.findall(p1, inp)[0]
    ctext = inp.split('}')[1]
    return (cflag, ctext)

dictionary = dictionary_score('quadgrams.txt')
cflag, ctext = read_ciphertext('ciphertext.txt')
bestkey = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
maxscore = -99e9
key_try_score, key_try = maxscore,bestkey[:]

print("we will try 3 times to find the most suitable key")
for i in range(3):
    print('%d th time...' %(i+1))
    random.shuffle(key_try)
    key_try_score = get_key_score(key_try,ctext,dictionary)            
    count = 0
    while count < 1000:
        key_swap = random_swap(key_try)
        key_swap_score = get_key_score(key_swap,ctext,dictionary)

        if key_swap_score > key_try_score:
            key_try_score = key_swap_score
            key_try = key_swap[:]
            count = 0
        
        count = count+1
    
    if key_try_score>maxscore:
        maxscore,bestkey = key_try_score,key_try[:]
        ss = SimpleSub(bestkey)

print('the key we find: ' +''.join(bestkey))
print ('my flag: '+ss.decipher(cflag))